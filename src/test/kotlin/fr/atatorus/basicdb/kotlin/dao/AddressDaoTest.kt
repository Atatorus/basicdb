package fr.atatorus.basicdb.kotlin.dao

import com.zaxxer.hikari.HikariDataSource
import fr.atatorus.basicdb.JDBC_URL
import fr.atatorus.basicdb.PASSWORD
import fr.atatorus.basicdb.USER
import fr.atatorus.basicdb.kotlin.datasource.DatasourceProvider
import fr.atatorus.basicdb.kotlin.dao.findAddresses
import fr.atatorus.basicdb.kotlin.dao.insertAddress
import fr.atatorus.basicdb.kotlin.dao.insertCustomer
import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import fr.atatorus.basicdb.kotlin.queries.updateQuery
import fr.atatorus.basicdb.kotlin.tx.SimpleTransactionProvider
import fr.atatorus.basicdb.kotlin.tx.Transaction
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

class AddressDaoTest {

    private val dsProvider = DatasourceProvider(HikariDataSource()) {
        this.jdbcUrl = JDBC_URL
        this.username = USER
        this.password = PASSWORD
    }
    private val txProvider = SimpleTransactionProvider(dsProvider.datasource)

    private lateinit var tx: Transaction
    private lateinit var customer: Customer

    @BeforeEach
    fun beforeEach() {
        tx = txProvider.getTransaction()
        customer = tx.execute {
            insertCustomer("Marcel", "Durat", "marcel@durat.com", LocalDate.now())
        }.getOrThrow()
        // On doit prendre une nouvelle transaction
        tx = txProvider.getTransaction()
    }

    @AfterEach
    fun afterEach() {
        // On ne peut plus rollbacker la transaction, on doit effacer les tables
        with(txProvider.getTransaction(true)) {
            connection.updateQuery("DELETE FROM ${fr.atatorus.basicdb.kotlin.models.Address.TABLE}")
            connection.updateQuery("DELETE FROM ${fr.atatorus.basicdb.kotlin.models.Customer.TABLE}")
            commit()
        }
    }

    @Test
    fun it_should_insert_and_get_address() {
        // Étant donné qu'on insère une adresse
        val result = tx.execute {
            insertAddress("Rue durat", "1418", "Duratbourg", "Duratland", customer.id)
        }

        // elle est comme attendue
        assertThat(result.isSuccess).isTrue
        val address = result.getOrThrow()
        with(address) {
            assertThat(id).isNotNull
            assertThat(street).isEqualTo("Rue durat")
            assertThat(zip).isEqualTo("1418")
            assertThat(city).isEqualTo("Duratbourg")
            assertThat(country).isEqualTo("Duratland")
            assertThat(customerId).isEqualTo(customer.id)
        }

        // Quand on la lit en base
        // La transaction a été commitée, il faut en prendre un autre
        tx = txProvider.getTransaction()
        val addresses = tx.execute {
            findAddresses(customer)
        }.getOrThrow()

        // Alors elle vaut ce qu'on attend
        assertThat(addresses).hasSize(1).element(0).usingRecursiveComparison().isEqualTo(address)
    }


}