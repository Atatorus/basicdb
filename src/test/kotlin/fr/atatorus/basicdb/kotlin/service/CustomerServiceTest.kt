package fr.atatorus.basicdb.kotlin.service

import com.zaxxer.hikari.HikariDataSource
import fr.atatorus.basicdb.JDBC_URL
import fr.atatorus.basicdb.PASSWORD
import fr.atatorus.basicdb.USER
import fr.atatorus.basicdb.kotlin.datasource.DatasourceProvider
import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import fr.atatorus.basicdb.kotlin.queries.updateQuery
import fr.atatorus.basicdb.kotlin.tx.SimpleTransactionProvider
import fr.atatorus.basicdb.kotlin.tx.TransactionProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

class CustomerServiceTest {

    private lateinit var dsProvider: DatasourceProvider<HikariDataSource>
    private lateinit var txProvider: TransactionProvider

    private lateinit var service: CustomerService

    @BeforeEach
    fun beforeEach() {
        dsProvider = DatasourceProvider(HikariDataSource()) {
            this.jdbcUrl = JDBC_URL
            this.username = USER
            this.password = PASSWORD
        }
        txProvider = SimpleTransactionProvider(dsProvider.datasource)
        clearDatabase()
        service = CustomerService(txProvider)
    }

    @AfterEach
    fun afterEach() {
        clearDatabase()
        dsProvider.datasource.close()
    }

    private fun clearDatabase() {
        with(txProvider.getTransaction(true)) {
            connection.updateQuery("DELETE FROM ${Address.TABLE}")
            connection.updateQuery("DELETE FROM ${Customer.TABLE}")
            commit()
        }
    }

    @Test
    fun it_should_create_and_get_customer_from_database() {
        // Étant donné qu'on a créé un customer
        val result = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3))

        // et qu'il est correct
        assertThat(result.isSuccess).isTrue
        val customer = result.getOrThrow()
        with(customer) {
            assertThat(id).isNotNull
            assertThat(firstName).isEqualTo("Marcel")
            assertThat(lastName).isEqualTo("Durat")
            assertThat(email).isEqualTo("marcel@durat.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }

        // Quand on le recherche en base
        val result2 = service.findCustomer("marcel@durat.fr")

        // Alors on le trouve
        assertThat(result2.isSuccess).isTrue
        val customer2 = result2.getOrThrow()
        assertThat(customer2).isNotNull
        with(customer2!!) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Marcel")
            assertThat(lastName).isEqualTo("Durat")
            assertThat(email).isEqualTo("marcel@durat.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }
    }

    @Test
    fun it_should_update_customer_in_database() {
        // Étant donné qu'on a créé un customer en base
        val result = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3))
        val customer = result.getOrThrow()

        // Quand on le met à jour
        with(service.updateCustomer(customer, "Henri", "Duraton", "henri@duraton.fr").getOrThrow()) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Henri")
            assertThat(lastName).isEqualTo("Duraton")
            assertThat(email).isEqualTo("henri@duraton.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }

        // Alors il est mis à jour en base
        val result2 = service.findCustomer("henri@duraton.fr")
        assertThat(result2.isSuccess).isTrue
        with(result2.getOrThrow()!!) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Henri")
            assertThat(lastName).isEqualTo("Duraton")
            assertThat(email).isEqualTo("henri@duraton.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }

    }

    @Test
    fun it_should_delete_customer_in_database() {
        // Étant donné créé un customer en base de données
        val customer = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3)).getOrThrow()

        // Quand le supprime
        service.deleteCustomer(customer)

        // Alors on ne le trouve plus en base
        val result = service.findCustomer("henri@duraton.fr")
        assertThat(result.isSuccess).isTrue
        assertThat(result.getOrThrow()).isNull()
    }


    @Test
    fun it_should_create_and_get_customer_with_adresses() {
        // Étant donné qu'on crée un customer et une adresse
        val creationDate = LocalDate.now()
        val createdCustomer = service.createCustomerWithAddress(
            "Marcel", "Durat", "marcel@durat.com",
            creationDate, "rue des Durats", "1418", "Duratbourg", "Duratland"
        ).getOrThrow()

        with(createdCustomer) {
            assertThat(createdCustomer.id).isNotNull
            assertThat(createdCustomer.firstName).isEqualTo("Marcel")
            assertThat(createdCustomer.lastName).isEqualTo("Durat")
            assertThat(createdCustomer.email).isEqualTo("marcel@durat.com")
            assertThat(createdCustomer.creationDate).isEqualTo(creationDate)
            assertThat(addresses).hasSize(1)
            with(addresses[0]) {
                assertThat(id).isNotNull
                assertThat(street).isEqualTo("rue des Durats")
                assertThat(zip).isEqualTo("1418")
                assertThat(city).isEqualTo("Duratbourg")
                assertThat(country).isEqualTo("Duratland")
            }
        }
        val address =
            service.createAddress(createdCustomer, "rue des Dusouris", "1418", "Dusourisville", "Dusourisland").getOrThrow()
        with(address) {
            assertThat(id).isNotNull
            assertThat(street).isEqualTo("rue des Dusouris")
            assertThat(zip).isEqualTo("1418")
            assertThat(city).isEqualTo("Dusourisville")
            assertThat(country).isEqualTo("Dusourisland")
        }
        createdCustomer.addAddress(address)

        // Quand on les cherche en base
        val result = service.findCustomerWithAddresses("marcel@durat.com")

        // Alors on obtient ce qu'on a sauvegardé
        assertThat(result.isSuccess).isTrue
        assertThat(result.getOrThrow()).usingRecursiveComparison().isEqualTo(createdCustomer)
    }
}