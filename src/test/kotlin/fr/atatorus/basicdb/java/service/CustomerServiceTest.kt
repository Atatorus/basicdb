package fr.atatorus.basicdb.java.service

import com.zaxxer.hikari.HikariDataSource
import fr.atatorus.basicdb.JDBC_URL
import fr.atatorus.basicdb.PASSWORD
import fr.atatorus.basicdb.USER
import fr.atatorus.basicdb.java.dao.AddressDao
import fr.atatorus.basicdb.java.dao.CustomerDao
import fr.atatorus.basicdb.java.datasource.DatasourceProvider
import fr.atatorus.basicdb.java.datasource.HikariDatasourceProvider
import fr.atatorus.basicdb.java.models.Address
import fr.atatorus.basicdb.java.models.CustomerRecord
import fr.atatorus.basicdb.java.queries.QueryExecutor
import fr.atatorus.basicdb.java.tx.IsolationLevel
import fr.atatorus.basicdb.java.tx.SimpleTransactionProvider
import fr.atatorus.basicdb.java.tx.TransactionProvider
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

class CustomerServiceTest {

    private lateinit var dsProvider: DatasourceProvider
    private lateinit var txProvider: TransactionProvider
    private lateinit var customerDao: CustomerDao
    private lateinit var addressDao: AddressDao
    private lateinit var service: CustomerService

    @BeforeEach
    fun beforeEach() {
        dsProvider =
            HikariDatasourceProvider(JDBC_URL, USER, PASSWORD)
        txProvider = SimpleTransactionProvider(dsProvider.datasource)
        clearDatabase()
        customerDao = CustomerDao()
        addressDao = AddressDao()
        service =
            CustomerService(txProvider, customerDao, addressDao)
    }

    @AfterEach
    fun afterEach() {
        clearDatabase()
        (dsProvider.datasource as HikariDataSource).close()
    }

    private fun clearDatabase() {
        with(txProvider.getTransaction(true, IsolationLevel.READ_COMMITTED)) {
            val executor = QueryExecutor(connection)
            executor.updateQuery("DELETE FROM ${Address.TABLE}")
            executor.updateQuery("DELETE FROM ${CustomerRecord.TABLE}")
            commit()
        }
    }

    @Test
    fun it_should_create_and_get_customer_from_database() {
        // Étant donné qu'on a créé un customer
        val customer: CustomerRecord = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3))
        // et qu'il est correct
        with(customer) {
            assertThat(id).isNotNull
            assertThat(firstName).isEqualTo("Marcel")
            assertThat(lastName).isEqualTo("Durat")
            assertThat(email).isEqualTo("marcel@durat.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }

        // Quand on le recherche en base
        val optional = service.findCustomer("marcel@durat.fr")

        // Alors on le trouve
        assertThat(optional).isPresent
        with(optional.get()) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Marcel")
            assertThat(lastName).isEqualTo("Durat")
            assertThat(email).isEqualTo("marcel@durat.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }
    }

    @Test
    fun it_should_update_customer_in_database() {
        // Étant donné qu'on a créé un customer en base
        val customer: CustomerRecord = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3))

        // Quand on le met à jour
        with(service.updateCustomer(customer, "Henri", "Duraton", "henri@duraton.fr")) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Henri")
            assertThat(lastName).isEqualTo("Duraton")
            assertThat(email).isEqualTo("henri@duraton.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }

        // Alors il est mis à jour en base
        val optional = service.findCustomer("henri@duraton.fr")
        assertThat(optional).isPresent
        with(optional.get()) {
            assertThat(id).isEqualTo(customer.id)
            assertThat(firstName).isEqualTo("Henri")
            assertThat(lastName).isEqualTo("Duraton")
            assertThat(email).isEqualTo("henri@duraton.fr")
            assertThat(creationDate).isEqualTo(LocalDate.of(2001, 2, 3))
        }
    }

    @Test
    fun it_should_delete_customer_in_database() {
        // Étant donné créé un customer en base de données
        val customer: CustomerRecord = service.createCustomer("Marcel", "Durat", "marcel@durat.fr", LocalDate.of(2001, 2, 3))

        // Quand le supprime
        service.deleteCustomer(customer)

        // Alors on ne le trouve plus en base
        val optional = service.findCustomer("henri@duraton.fr")
        assertThat(optional).isEmpty
    }


    @Test
    fun it_should_create_and_get_customer_with_adresses() {
        // Étant donné qu'on crée un customer et une adresse
        val creationDate = LocalDate.now()
        val customer = service.createCustomerWithAddress(
            "Marcel", "Durat", "marcel@durat.com",
            creationDate, "rue des Durats", "1418", "Duratbourg", "Duratland"
        )

        with(customer) {
            assertThat(id).isNotNull
            assertThat(firstName).isEqualTo("Marcel")
            assertThat(lastName).isEqualTo("Durat")
            assertThat(email).isEqualTo("marcel@durat.com")
            assertThat(creationDate).isEqualTo(creationDate)
            assertThat(addresses).hasSize(1)
            with(addresses[0]) {
                assertThat(id).isNotNull
                assertThat(street).isEqualTo("rue des Durats")
                assertThat(zip).isEqualTo("1418")
                assertThat(city).isEqualTo("Duratbourg")
                assertThat(country).isEqualTo("Duratland")
            }
        }
        val address1 = customer.addresses[0]
        val address2 = service.createAddress(customer, "rue des Dusouris", "1418", "Dusourisville", "Dusourisland")

        // Quand on les cherche en base
        val optionalCustomer = service.findCustomerWithAddresses("marcel@durat.com")

        // Alors on obtient ce qu'on a sauvegardé
        assertThat(optionalCustomer).isNotEmpty
        with(optionalCustomer.get()) {
            assertThat(customer).usingRecursiveComparison().isEqualTo(customer)
            assertThat(addresses[0]).usingRecursiveComparison().isEqualTo(address1)
            assertThat(addresses[1]).usingRecursiveComparison().isEqualTo(address2)
        }
    }

}