package fr.atatorus.basicdb.java.dao

import fr.atatorus.basicdb.JDBC_URL
import fr.atatorus.basicdb.PASSWORD
import fr.atatorus.basicdb.USER
import fr.atatorus.basicdb.java.datasource.HikariDatasourceProvider
import fr.atatorus.basicdb.java.models.CustomerRecord
import fr.atatorus.basicdb.java.tx.IsolationLevel
import fr.atatorus.basicdb.java.tx.SimpleTransactionProvider
import fr.atatorus.basicdb.java.tx.Transaction
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

class AddressDaoTest {

    private val dsProvider =
        HikariDatasourceProvider(JDBC_URL, USER, PASSWORD)
    private val txProvider =
        SimpleTransactionProvider(dsProvider.datasource)

    private lateinit var customerDao: CustomerDao
    private lateinit var dao: AddressDao

    private lateinit var tx: Transaction
    private lateinit var customer: CustomerRecord

    @BeforeEach
    fun beforeEach() {
        tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        customerDao = CustomerDao()
        customer = customerDao.insertCustomer(tx, "Marcel", "Durat", "marcel@durat.com", LocalDate.now())
        dao = AddressDao()
    }

    @AfterEach
    fun afterEach() {
        tx.rollback()
    }

    @Test
    fun it_should_insert_and_get_address() {
        // Étant donné qu'on insère une adresse
        val address = dao.insertAddress(tx, "Rue durat", "1418", "Duratbourg", "Duratland", customer.id)

        // elle est comme attendue
        with(address) {
            assertThat(id).isNotNull
            assertThat(street).isEqualTo("Rue durat")
            assertThat(zip).isEqualTo("1418")
            assertThat(city).isEqualTo("Duratbourg")
            assertThat(country).isEqualTo("Duratland")
            assertThat(customerId).isEqualTo(customer.id)
        }

        // Quand on la lit en base
        val addresses = dao.findAddresses(tx, customer)

        // Alors elle vaut ce qu'on attend
        assertThat(addresses).hasSize(1).element(0).usingRecursiveComparison().isEqualTo(address)
    }


}