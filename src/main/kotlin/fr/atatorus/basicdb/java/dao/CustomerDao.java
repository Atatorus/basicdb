package fr.atatorus.basicdb.java.dao;

import fr.atatorus.basicdb.java.exceptions.BasicdbException;
import fr.atatorus.basicdb.java.mapper.CustomerMapper;
import fr.atatorus.basicdb.java.mapper.CustomerExtractor;
import fr.atatorus.basicdb.java.models.Address;
import fr.atatorus.basicdb.java.models.CustomerRecord;
import fr.atatorus.basicdb.java.queries.QueryExecutor;
import fr.atatorus.basicdb.java.tx.Transaction;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CustomerDao {

    private final CustomerMapper mapper = new CustomerMapper();

    public CustomerRecord insertCustomer(Transaction tx,
                                         String firstName,
                                         String lastName,
                                         String email,
                                         LocalDate creationDate) throws SQLException, BasicdbException {
        String sql = "INSERT INTO " + CustomerRecord.TABLE + " (" +
                     "first_name, last_name, email, creation_date" +
                     ") VALUES (" +
                     "?, ?, ?, ?" +
                     ")";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        Long id = executor.insertQuery(sql, Arrays.asList(firstName, lastName, email, creationDate), "id");
        return new CustomerRecord(id, firstName, lastName, email, creationDate);
    }

    public CustomerRecord updateCustomer(Transaction tx, CustomerRecord customer, String newFirstName, String newLastName, String newEmail) throws SQLException {
        String sql = "UPDATE " + CustomerRecord.TABLE + " SET " +
                     "first_name = ?, last_name = ?, email = ? " +
                     "WHERE id = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        Integer updatedRows = executor.updateQuery(sql,
                newFirstName, newLastName, newEmail, customer.id());
        if (updatedRows == 0) {
            throw new RuntimeException("Le customer n'existe plus en base de données");
        }
        return new CustomerRecord(customer.id(), newFirstName, newLastName, newEmail, customer.creationDate());
    }

    public Optional<CustomerRecord> findCustomer(Transaction tx, String email) throws SQLException {
        String sql = "SELECT id, first_name, last_name, email, creation_date " +
                     "FROM " + CustomerRecord.TABLE +
                     " WHERE email = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        List<CustomerRecord> customers = executor.queryForObjects(sql, Collections.singletonList(email), mapper);
        if (customers.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(customers.get(0));
    }

    public void deleteCustomer(Transaction tx, CustomerRecord customer) throws SQLException {
        String sql = "DELETE FROM " + CustomerRecord.TABLE + " WHERE id = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        executor.updateQuery(sql, customer.id());
    }

    public List<CustomerRecord> findCustomerWithAddresses(Transaction tx, String email) throws SQLException {
        String sql = "SELECT c.id AS cid, c.first_name AS cfirst_name, c.last_name AS clast_name, c.email AS cemail, c.creation_date AS ccreation_date," +
                     "a.id AS aid, a.street AS astreet, a.zip AS azip, a.city AS acity, a.country AS acountry, a.customer_id AS acustomer_id " +
                     "FROM " + CustomerRecord.TABLE + " c " +
                     "LEFT OUTER JOIN " + Address.TABLE + " a ON a.customer_id = c.id " +
                     "WHERE c.email = ? " +
                     "ORDER BY aid";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        return executor.queryForObjects(sql, Collections.singletonList(email), new CustomerExtractor());

    }

}
