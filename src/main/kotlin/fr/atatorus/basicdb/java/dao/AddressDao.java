package fr.atatorus.basicdb.java.dao;

import fr.atatorus.basicdb.java.exceptions.BasicdbException;
import fr.atatorus.basicdb.java.mapper.AddressMapper;
import fr.atatorus.basicdb.java.models.Address;
import fr.atatorus.basicdb.java.models.CustomerRecord;
import fr.atatorus.basicdb.java.queries.QueryExecutor;
import fr.atatorus.basicdb.java.tx.Transaction;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AddressDao {

    public AddressMapper addressMapper = new AddressMapper();

    public Address insertAddress(Transaction tx,
                                 String street,
                                 String zip,
                                 String city,
                                 String country,
                                 Long customerId) throws SQLException, BasicdbException {
        String sql = "INSERT INTO address (" +
                     "   street, zip, city, country, customer_id" +
                     ") VALUES (" +
                     "   ?, ?, ?, ?, ?" +
                     ")";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        Long id = executor.insertQuery(sql, Arrays.asList(street, zip, city, country, customerId), "id");
        return new Address(id, street, zip, city, country, customerId);
    }

    public Address updateAddress(Transaction tx,
                                 Address address,
                                 String newStreet,
                                 String newZip,
                                 String newCity,
                                 String newCountry) throws SQLException {
        String sql = "UPDATE " + Address.TABLE + " SET " +
                     "street = ?, zip = ?, city = ?, country = ? " +
                     "WHERE id = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        Integer updatedRows = executor.updateQuery(sql, newStreet, newZip, newCity, newCountry);
        if (updatedRows == 0) {
            throw new RuntimeException("L'addresse n'existe plus en base de données.");
        }
        return new Address(address.id(), newStreet, newZip, newCity, newCountry, address.customerId());
    }

    public List<Address> findAddresses(Transaction tx, CustomerRecord customer) throws SQLException {
        String sql = "SELECT id, street, zip, city, country, customer_id " +
                     "FROM " + Address.TABLE +
                     " WHERE customer_id = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        return executor.queryForObjects(sql, Collections.singletonList(customer.id()), addressMapper);
    }

    public void deleteAddress(Transaction tx, Address address) throws SQLException {
        String sql = "DELETE FROM " + Address.TABLE + " WHERE id = ?";
        QueryExecutor executor = new QueryExecutor(tx.getConnection());
        executor.updateQuery(sql, address.id());
    }

}
