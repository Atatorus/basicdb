package fr.atatorus.basicdb.java.queries;

import fr.atatorus.basicdb.java.exceptions.BasicdbException;
import fr.atatorus.basicdb.java.mapper.Extractor;
import fr.atatorus.basicdb.java.mapper.Mapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueryExecutor {

    private final Connection connection;

    public QueryExecutor(Connection connection) {
        this.connection = connection;
    }

    /**
     * Exécute une requête INSERT
     *
     * @param sql        La requête à exécuter
     * @param parameters les paramètres de la requête
     * @param key        le nom de la clé primaire auto générée
     * @return la clé primaire auto générée
     */
    public Long insertQuery(String sql, List<Object> parameters, String key) throws SQLException, BasicdbException {
        try (PreparedStatement stmt = statement(sql, parameters, key)) {
            stmt.executeUpdate();
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(key);
            } else {
                throw new BasicdbException("Insert error");
            }
        }
    }

    /**
     * Execute une requête UPDATE ou DELETE
     *
     * @param sql        la requête a exécuter
     * @param parameters les paramètres
     * @return le nombre d'enregistrements mis à jour ou supprimés
     * @throws SQLException en cas d'erreur SQL
     */
    public Integer updateQuery(String sql, Object... parameters) throws SQLException {
        try (PreparedStatement stmt = statement(sql, Arrays.asList(parameters))) {
            return stmt.executeUpdate();
        }
    }

    /**
     * Exécute une requête SELECT pour récupérer une liste d'objets. Les objets seront créer à l'aide d'un {@link Mapper}
     * qui va construire les objets en parcourant les rangs du {@link ResultSet}
     * @param sql La requête
     * @param parameters Les paramètres de la requête
     * @param mapper Un {@link Mapper} qui va créer les objets
     * @return La liste des objets trouvés en base
     * @param <T> Le type d'objet à retourner
     * @throws SQLException En cas d'erreur SQL
     */
    public <T> List<T> queryForObjects(String sql,
                                       List<Object> parameters,
                                       Mapper<T> mapper) throws SQLException {
        try (PreparedStatement stmt = statement(sql, parameters)) {
            List<T> result = new ArrayList<>();
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                result.add(mapper.mapTo(rs));
            }
            return result;
        }
    }

    /**
     * Exécute une requête SELECT pour récupérer une liste d'objet. La liste sera créée à l'aide d'un {@link Extractor}
     * qui va parcourir les rangs du {@link ResultSet} pour construire les objets à retourner.
     * @param sql La requête SQL
     * @param parameters Les paramètres de la requête
     * @param extractor L'extractor qui va parcourir le result set et construire les objets
     * @return La liste des objets trouvés en base
     * @param <T> Le type d'objet à renvoyer
     * @throws SQLException En cas d'erreur SQL
     */
    public <T> List<T> queryForObjects(String sql,
                                       List<Object> parameters,
                                       Extractor<T> extractor) throws SQLException {
        try (PreparedStatement stmt = statement(sql, parameters)) {
            ResultSet rs = stmt.executeQuery();
            return extractor.extractList(rs);
        }
    }

    private PreparedStatement statement(String sql,
                                        List<Object> parameters) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(sql,
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY,
                ResultSet.CLOSE_CURSORS_AT_COMMIT);
        for (int i = 0; i < parameters.size(); i++) {
            stmt.setObject(i + 1, parameters.get(i));
        }
        return stmt;
    }

    private PreparedStatement statement(String sql,
                                        List<Object> parameters,
                                        String key) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(sql, new String[]{key});
        for (int i = 0; i < parameters.size(); i++) {
            stmt.setObject(i + 1, parameters.get(i));
        }
        return stmt;
    }

}
