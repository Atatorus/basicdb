package fr.atatorus.basicdb.java.tx;

import java.sql.Connection;
import java.sql.SQLException;

public interface Transaction {

    Connection getConnection();

    void commit() throws SQLException;

    void rollback() throws SQLException;

    boolean isClosed() throws SQLException;

}
