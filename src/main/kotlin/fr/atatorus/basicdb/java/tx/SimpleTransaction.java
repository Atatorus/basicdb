package fr.atatorus.basicdb.java.tx;

import java.sql.Connection;
import java.sql.SQLException;

public class SimpleTransaction implements Transaction {

    private final Connection connection;

    public SimpleTransaction(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Connection getConnection() {
        return this.connection;
    }

    public void commit() throws SQLException {
        if (!connection.getAutoCommit()) {
            connection.commit();
        }
        connection.close();
    }

    public void rollback() throws SQLException {
        if (!connection.getAutoCommit()) {
            connection.rollback();
        }
        connection.close();
    }

    public boolean isClosed() throws SQLException {
        return connection.isClosed();
    }
}
