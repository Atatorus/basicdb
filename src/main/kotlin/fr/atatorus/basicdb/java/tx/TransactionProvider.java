package fr.atatorus.basicdb.java.tx;

import fr.atatorus.basicdb.java.exceptions.BasicdbException;

public interface TransactionProvider {

    Transaction getTransaction(boolean autocommit, IsolationLevel isolation) throws BasicdbException;

}
