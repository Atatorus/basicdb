package fr.atatorus.basicdb.java.tx;

import fr.atatorus.basicdb.java.exceptions.BasicdbException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class SimpleTransactionProvider implements TransactionProvider {

    private final DataSource dataSource;

    public SimpleTransactionProvider(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Transaction getTransaction(boolean autocommit, IsolationLevel isolationLevel) throws BasicdbException {
        if (isolationLevel == IsolationLevel.NO_TRANSACTION) {
            throw new BasicdbException("Le niveau d'isolation NO_TRANSACTION n'est pas supporté.");
        }
        try {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(autocommit);
            connection.setTransactionIsolation(isolationLevel.level);
            return new SimpleTransaction(connection);
        } catch (SQLException e) {
            throw new BasicdbException(e);
        }
    }
}
