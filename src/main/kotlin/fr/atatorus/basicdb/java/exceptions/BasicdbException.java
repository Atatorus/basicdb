package fr.atatorus.basicdb.java.exceptions;

public class BasicdbException extends Exception {

    public BasicdbException(Throwable cause) {
        super(cause);
    }

    public BasicdbException(String message) {
        super(message);
    }

    public BasicdbException(String message, Throwable cause) {
        super(message, cause);
    }
}
