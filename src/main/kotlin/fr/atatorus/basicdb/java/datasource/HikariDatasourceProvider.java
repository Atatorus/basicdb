package fr.atatorus.basicdb.java.datasource;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class HikariDatasourceProvider implements DatasourceProvider {

    private final HikariDataSource dataSource;

    public HikariDatasourceProvider(String jdbcUrl, String user, String password) {
        this.dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(jdbcUrl);
        dataSource.setUsername(user);
        dataSource.setPassword(password);
    }

    public DataSource getDatasource() {
        return this.dataSource;
    }
}
