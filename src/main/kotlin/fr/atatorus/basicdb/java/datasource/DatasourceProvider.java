package fr.atatorus.basicdb.java.datasource;

import javax.sql.DataSource;

public interface DatasourceProvider {

    DataSource getDatasource();
}
