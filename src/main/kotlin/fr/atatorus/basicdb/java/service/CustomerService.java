package fr.atatorus.basicdb.java.service;

import fr.atatorus.basicdb.java.dao.AddressDao;
import fr.atatorus.basicdb.java.dao.CustomerDao;
import fr.atatorus.basicdb.java.exceptions.BasicdbException;
import fr.atatorus.basicdb.java.models.Address;
import fr.atatorus.basicdb.java.models.CustomerRecord;
import fr.atatorus.basicdb.java.tx.IsolationLevel;
import fr.atatorus.basicdb.java.tx.TransactionProvider;
import fr.atatorus.basicdb.java.tx.Transaction;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * CRUD operations sur les {@link CustomerRecord}s
 */
public class CustomerService {

    private final TransactionProvider txProvider;
    private final CustomerDao customerDao;
    private final AddressDao addressDao;

    public CustomerService(TransactionProvider txProvider, CustomerDao customerDao, AddressDao addressDao) {
        this.txProvider = txProvider;
        this.customerDao = customerDao;
        this.addressDao = addressDao;
    }

    public CustomerRecord createCustomer(String firstname,
                                         String lastName,
                                         String email,
                                         LocalDate creationDate) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            CustomerRecord customer = customerDao.insertCustomer(tx, firstname, lastName, email, creationDate);
            tx.commit();
            return customer;
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return null;
    }

    public CustomerRecord updateCustomer(CustomerRecord customer,
                                         String newFirstName,
                                         String newLastName,
                                         String newEmail) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            CustomerRecord newCustomer = customerDao.updateCustomer(tx, customer, newFirstName, newLastName, newEmail);
            tx.commit();
            return newCustomer;
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return null;
    }

    public Optional<CustomerRecord> findCustomer(String email) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            Optional<CustomerRecord> customer = customerDao.findCustomer(tx, email);
            tx.commit();
            return customer;
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return Optional.empty();
    }

    public void deleteCustomer(CustomerRecord customer) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            customerDao.deleteCustomer(tx, customer);
            tx.commit();
        } catch (SQLException e) {
            rollback(tx, e);
        }

    }

    public CustomerRecord createCustomerWithAddress(String firstName,
                                                    String lastName,
                                                    String email,
                                                    LocalDate creationDate,
                                                    String street,
                                                    String zip,
                                                    String city,
                                                    String country) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            CustomerRecord customer = customerDao.insertCustomer(tx, firstName, lastName, email, creationDate);
            Address address = addressDao.insertAddress(tx, street, zip, city, country, customer.id());
            customer.addAddress(address);
            tx.commit();
            return customer;
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return null;
    }

    public Address createAddress(CustomerRecord customer,
                                 String street,
                                 String zip,
                                 String city,
                                 String country) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            Address address = addressDao.insertAddress(tx, street, zip, city, country, customer.id());
            tx.commit();
            return address;
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return null;
    }

    public Optional<CustomerRecord> findCustomerWithAddresses(String email) throws BasicdbException {
        Transaction tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED);
        try {
            List<CustomerRecord> cwas = customerDao.findCustomerWithAddresses(tx, email);
            if (cwas.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(cwas.get(0));
            }
        } catch (SQLException e) {
            rollback(tx, e);
        }
        return Optional.empty();
    }

    private void rollback(Transaction tx, SQLException e) throws BasicdbException {
        try {
            tx.rollback();
        } catch (SQLException ex) {
            throw new BasicdbException(ex);
        }
        throw new BasicdbException(e);
    }

}
