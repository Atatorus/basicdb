package fr.atatorus.basicdb.java.models;

public record Address(Long id, String street, String zip, String city, String country, Long customerId) {

    public static  final String TABLE = "address";
}
