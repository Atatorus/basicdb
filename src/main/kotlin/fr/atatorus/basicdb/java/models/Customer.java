package fr.atatorus.basicdb.java.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDate creationDate;
    private List<Address> addresses;

    public Customer(Long id,
                    String firstName,
                    String lastName,
                    String email,
                    LocalDate creationDate,
                    List<Address> addresses) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.creationDate = creationDate;
        this.addresses = addresses;
    }

    public Customer(Long id, String firstName, String lastName, String email, LocalDate creationDate) {
        this(id, firstName, lastName, email, creationDate, new ArrayList<>());
    }

    public Customer(Long id, String firstName, String lastName, String email) {
        this(id, firstName, lastName, email, LocalDate.now(), new ArrayList<>());
    }

    public Long getId() {
        return id;
    }

    public Customer withId(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Customer withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Customer withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Customer withEmail(String email) {
        this.email = email;
        return this;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Customer withCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public Customer withAddresses(List<Address> addresses) {
        this.addresses = addresses;
        return this;
    }

    public Customer addAddress(Address address) {
        this.addresses.add(address);
        return this;
    }

}
