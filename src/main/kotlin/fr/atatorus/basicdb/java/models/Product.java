package fr.atatorus.basicdb.java.models;

public record Product(Long id, String name, String description, Double price) {

    public static final String TABLE = "product";
}
