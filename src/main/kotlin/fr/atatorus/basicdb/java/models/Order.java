package fr.atatorus.basicdb.java.models;

import java.time.LocalDate;

public record Order(Long id, LocalDate orderDate, Long deliveryAddressId) {

    public static final String TABLE = "orders";

}
