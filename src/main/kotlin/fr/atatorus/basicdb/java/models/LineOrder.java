package fr.atatorus.basicdb.java.models;

public record LineOrder(Long id, Integer rank, Long productId, Integer quantity, Long orderId) {

    public static final String TABLE = "line_order";

}
