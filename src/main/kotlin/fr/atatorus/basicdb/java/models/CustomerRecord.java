package fr.atatorus.basicdb.java.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public record CustomerRecord(Long id, String firstName, String lastName, String email, LocalDate creationDate, List<Address> addresses) {

    public CustomerRecord(Long id, String firstName, String lastName, String email, LocalDate creationDate) {
        this(id, firstName, lastName, email, creationDate, new ArrayList<>());
    }

    public void addAddress(Address address) {
        this.addresses.add(address);
    }

    public static final String TABLE = "customer";

}
