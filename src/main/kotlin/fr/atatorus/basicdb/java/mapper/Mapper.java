package fr.atatorus.basicdb.java.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Mapper<T> {

    T mapTo(ResultSet rs) throws SQLException;

}
