package fr.atatorus.basicdb.java.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Extractor<T> {

    List<T> extractList(ResultSet rs) throws SQLException;

}
