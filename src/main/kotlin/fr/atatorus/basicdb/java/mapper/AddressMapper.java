package fr.atatorus.basicdb.java.mapper;

import fr.atatorus.basicdb.java.models.Address;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AddressMapper implements Mapper<Address> {

    private final String prefix;

    public AddressMapper() {
        this("");
    }

    public AddressMapper(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public Address mapTo(ResultSet rs) throws SQLException {
        Long id = rs.getLong(prefix + "id");
        if (rs.wasNull()) {
            return null;
        }
        String street = rs.getString(prefix + "street");
        String zip = rs.getString(prefix + "zip");
        String city = rs.getString(prefix + "city");
        String country = rs.getString(prefix + "country");
        if (rs.wasNull()) {
            country = null;
        }
        Long customerId = rs.getLong(prefix + "customer_id");

        return new Address(id, street,zip, city, country, customerId);
    }
}
