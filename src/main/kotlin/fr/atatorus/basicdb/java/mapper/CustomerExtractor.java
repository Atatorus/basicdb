package fr.atatorus.basicdb.java.mapper;

import fr.atatorus.basicdb.java.models.Address;
import fr.atatorus.basicdb.java.models.CustomerRecord;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerExtractor implements Extractor<CustomerRecord> {

    public List<CustomerRecord> extractList(ResultSet rs) throws SQLException {
        List<CustomerRecord> customers = new ArrayList<>();
        CustomerRecord currentCustomer = null;
        CustomerMapper customerMapper = new CustomerMapper("c");
        AddressMapper addressMapper = new AddressMapper("a");
        while(rs.next()) {
            Long cId = rs.getLong("cid");
            if (currentCustomer == null || !cId.equals(currentCustomer.id())) {
                currentCustomer =  customerMapper.mapTo(rs);
                customers.add(currentCustomer);
            }
            Address address = addressMapper.mapTo(rs);
            if (address != null) {
                currentCustomer.addAddress(address);
            }
        }
        return customers;
    }

}
