package fr.atatorus.basicdb.java.mapper;

import fr.atatorus.basicdb.java.models.CustomerRecord;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class CustomerMapper implements Mapper<CustomerRecord> {

    private final String prefix;

    public CustomerMapper() {
        this("");
    }

    public CustomerMapper(String prefix) {
        this.prefix = prefix;
    }

    public CustomerRecord mapTo(ResultSet rs) throws SQLException {
        Long id = rs.getLong(prefix + "id");
        String firstName = rs.getString(prefix + "first_name");
        if (rs.wasNull()) {
            firstName = null;
        }
        String lastName = rs.getString(prefix + "last_name");
        if (rs.wasNull()) {
            lastName = null;
        }
        String email = rs.getString(prefix + "email");
        LocalDate creationDate = rs.getDate(prefix + "creation_date").toLocalDate();
        return new CustomerRecord(id, firstName, lastName, email, creationDate);
    }

}
