package fr.atatorus.basicdb.kotlin.mapper

import fr.atatorus.basicdb.kotlin.models.Customer
import java.sql.ResultSet

fun customersExtractor(customerPrefix: String = "c", addressPrefix: String = "a"): (ResultSet) -> List<Customer> = { rs ->
    val customers = mutableListOf<Customer>()
    val customerMapper = customerMapper(customerPrefix)
    val addressMapper = addressMapper(addressPrefix)
    var currentCustomer: Customer? = null

    while(rs.next()) {
        val customer = customerMapper.invoke(rs)
        if (currentCustomer == null || customer.id != currentCustomer.id) {
            currentCustomer = customer
            customers += currentCustomer
        }
        rs.getLong("${addressPrefix}id")
        if (!rs.wasNull()) {
            val address = addressMapper.invoke(rs)
            currentCustomer.addAddress(address)
        }
    }
    customers
}