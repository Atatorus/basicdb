package fr.atatorus.basicdb.kotlin.mapper

import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*

@Throws(SQLException::class)
fun addressMapper(prefix: String = ""): (ResultSet) -> Address = { rs ->
    val id = rs.getLong("${prefix}id")
    val street = rs.getString("${prefix}street")
    val zip = rs.getString("${prefix}zip")
    val city = rs.getString("${prefix}city")
    var country = rs.getString("${prefix}country")
    if (rs.wasNull()) {
        country = null
    }
    val customerId = rs.getLong("${prefix}customer_id")
    Address(id, street, zip, city, country, customerId)
}

fun customerMapper(prefix: String = ""): (ResultSet) -> Customer = { rs ->
    val id = rs.getLong("${prefix}id")
    var firstName = rs.getString("${prefix}first_name")
    if (rs.wasNull()) {
        firstName = null
    }
    var lastName = rs.getString("${prefix}last_name")
    if (rs.wasNull()) {
        lastName = null
    }
    val email = rs.getString("${prefix}email")
    val creationDate = rs.getDate("${prefix}creation_date").toLocalDate()
    Customer(id, firstName, lastName, email, creationDate)
}