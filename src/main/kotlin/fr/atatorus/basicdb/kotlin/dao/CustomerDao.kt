package fr.atatorus.basicdb.kotlin.dao

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException
import fr.atatorus.basicdb.kotlin.mapper.customerMapper
import fr.atatorus.basicdb.kotlin.mapper.customersExtractor
import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import fr.atatorus.basicdb.kotlin.queries.insertQuery
import fr.atatorus.basicdb.kotlin.queries.queryAndExtractObjects
import fr.atatorus.basicdb.kotlin.queries.queryAndMapObjects
import fr.atatorus.basicdb.kotlin.queries.updateQuery
import java.sql.Connection
import java.sql.SQLException
import java.time.LocalDate
import java.util.*

@Throws(SQLException::class, BasicdbException::class)
fun Connection.insertCustomer(
    firstName: String?,
    lastName: String?,
    email: String,
    creationDate: LocalDate
): Customer {
    val sql = """
        INSERT INTO ${Customer.TABLE} (
            first_name, last_name, email, creation_date
        ) VALUES (
            ?, ?, ?, ?
        )
    """.trimIndent()
    val id = insertQuery(sql, "id", firstName, lastName, email, creationDate)
    return Customer(id, firstName, lastName, email, creationDate)
}

@Throws(SQLException::class)
fun Connection.updateCustomer(
    customer: Customer,
    newFirstName: String? = customer.firstName,
    newLastName: String? = customer.lastName,
    newEmail: String = customer.email
): Customer {
    val sql = """
        UPDATE ${Customer.TABLE} SET
        first_name = ?, last_name = ?, email = ?
        WHERE id = ?
    """.trimIndent()
    val updatedRows = updateQuery(sql, newFirstName, newLastName, newEmail, customer.id)
    if (updatedRows == 0) {
        throw RuntimeException("Le customer n'existe plus en base de données")
    }
    return customer.copy(firstName =  newFirstName, lastName =  newLastName, email = newEmail)
}

@Throws(SQLException::class)
fun Connection.findCustomer(email: String): Customer? {
    val sql = """
        SELECT id, first_name, last_name, email, creation_date 
        FROM ${Customer.TABLE}
        WHERE email = ?
    """.trimIndent()
    return queryAndMapObjects(sql, customerMapper(""), email).firstOrNull()
}

@Throws(SQLException::class)
fun Connection.deleteCustomer(customer: Customer) {
    val sql = "DELETE FROM ${Customer.TABLE} WHERE id = ?"
    updateQuery(sql, customer.id)
}

@Throws(SQLException::class)
fun Connection.findCustomerWithAddresses(email: String): List<Customer> {
    val sql = """
        SELECT c.id AS cid, c.first_name AS cfirst_name, c.last_name AS clast_name, c.email AS cemail, c.creation_date AS ccreation_date,
               a.id AS aid, a.street AS astreet, a.zip AS azip, a.city AS acity, a.country AS acountry, a.customer_id as acustomer_id 
        FROM ${Customer.TABLE} c
        JOIN ${Address.TABLE} a ON a.customer_id = c.id
        WHERE c.email = ?
        ORDER BY aid
    """.trimIndent()
    return queryAndExtractObjects(sql, customersExtractor("c", "a"), email)
}