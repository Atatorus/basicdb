package fr.atatorus.basicdb.kotlin.dao

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException
import fr.atatorus.basicdb.kotlin.mapper.addressMapper
import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import fr.atatorus.basicdb.kotlin.queries.insertQuery
import fr.atatorus.basicdb.kotlin.queries.queryAndMapObjects
import fr.atatorus.basicdb.kotlin.queries.updateQuery
import java.sql.Connection
import java.sql.SQLException

@Throws(SQLException::class, BasicdbException::class)
fun Connection.insertAddress(
    street: String,
    zip: String,
    city: String,
    country: String?,
    customerId: Long
): Address {
    val sql = """
        INSERT INTO ${Address.TABLE} (
            street, zip, city, country, customer_id
        ) VALUES (
            ?, ?, ?, ?, ?
        )
    """.trimIndent()
    val id = insertQuery(sql, "id", street, zip, city, country!!, customerId)
    return Address(id, street, zip, city, country, customerId)
}

@Throws(SQLException::class)
fun Connection.updateAddress(
    address: Address,
    newStreet: String,
    newZip: String,
    newCity: String,
    newCountry: String?
): Address {
    val sql = """
        UPDATE ${Address.TABLE}  SET
            street = ?, zip = ?, city = ?, country = ?
        WHERE id = ?
    """.trimIndent()
    val updatedRows = updateQuery(sql, newStreet, newZip, newCity, newCountry)
    if (updatedRows == 0) {
        throw RuntimeException("L'adresse n'existe plus en base de données.")
    }
    return Address(address.id, newStreet, newZip, newCity, newCountry, address.customerId)
}

@Throws(SQLException::class)
fun Connection.findAddresses(customer: Customer): List<Address> {
    val sql = """
        SELECT id, street, zip, city, country, customer_id
        FROM ${Address.TABLE}
        WHERE customer_id = ?
    """.trimIndent()
    return queryAndMapObjects(sql, addressMapper(""), customer.id)
}

@Throws(SQLException::class)
fun Connection.deleteAddress(address: Address) {
    val sql = "DELETE FROM " + Address.TABLE + " WHERE id = ?"
    updateQuery(sql, address.id)
}