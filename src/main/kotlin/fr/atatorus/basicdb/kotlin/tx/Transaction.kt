package fr.atatorus.basicdb.kotlin.tx

import java.sql.Connection
import java.sql.SQLException

interface Transaction {
    val connection: Connection

    /**
     * Execute une requête, et rollback la transaction en cas d'erreur
     */
    fun <T> execute(query: Connection.() -> T): Result<T>

    @Throws(SQLException::class)
    fun commit()

    @Throws(SQLException::class)
    fun rollback()

    @get:Throws(SQLException::class)
    val isClosed: Boolean
}