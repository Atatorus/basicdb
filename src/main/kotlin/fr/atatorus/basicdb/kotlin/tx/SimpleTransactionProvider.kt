package fr.atatorus.basicdb.kotlin.tx

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException
import java.sql.SQLException
import javax.sql.DataSource

class SimpleTransactionProvider(private val dataSource: DataSource) : TransactionProvider {

    @Throws(BasicdbException::class)
    override fun getTransaction(autocommit: Boolean, isolationLevel: IsolationLevel): Transaction {
        if (isolationLevel === IsolationLevel.NO_TRANSACTION) {
            throw BasicdbException("Le niveau d'isolation NO_TRANSACTION n'est pas supporté.")
        }
        return try {
            val connection = dataSource.connection
            connection.autoCommit = autocommit
            connection.transactionIsolation = isolationLevel.level
            SimpleTransaction(connection)
        } catch (e: SQLException) {
            throw BasicdbException(e)
        }
    }
}