package fr.atatorus.basicdb.kotlin.tx

import java.sql.Connection
import java.sql.SQLException

class SimpleTransaction(override val connection: Connection) : Transaction {

    override fun <T> execute(query: Connection.() -> T): Result<T> {
        return try {
            Result.success(connection.query()).also {
                commit()
            }
        } catch (e: Exception) {
            rollback()
            Result.failure(e)
        }
    }

    @Throws(SQLException::class)
    override fun commit() {
        if (!connection.autoCommit) {
            connection.commit()
        }
        connection.close()
    }

    @Throws(SQLException::class)
    override fun rollback() {
        if (!connection.autoCommit) {
            connection.rollback()
        }
        connection.close()
    }

    @get:Throws(SQLException::class)
    override val isClosed: Boolean
        get() = connection.isClosed
}