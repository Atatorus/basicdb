package fr.atatorus.basicdb.kotlin.tx

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException

interface TransactionProvider {

    /**
     * @param autocommit false par défaut
     * @param isolationLevel READ_COMMITTED par défaut
     * @return la transaction
     * @throws BasicdbException si quelque chose se passe mal
     */
    @Throws(BasicdbException::class)
    fun getTransaction(autocommit: Boolean = false, isolationLevel: IsolationLevel = IsolationLevel.READ_COMMITTED): Transaction
}