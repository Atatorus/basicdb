package fr.atatorus.basicdb.kotlin.tx

import java.sql.Connection

enum class IsolationLevel(
    val level: Int
) {
    NO_TRANSACTION(Connection.TRANSACTION_NONE),  // 0
    READ_UNCOMMITTED(Connection.TRANSACTION_READ_UNCOMMITTED),  // 1
    READ_COMMITTED(Connection.TRANSACTION_READ_COMMITTED),  // 2
    REPEATABLE_READ(Connection.TRANSACTION_REPEATABLE_READ),  // 4
    SERIALIZABLE(Connection.TRANSACTION_SERIALIZABLE) // 8
}