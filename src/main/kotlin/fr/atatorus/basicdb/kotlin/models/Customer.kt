package fr.atatorus.basicdb.kotlin.models

import java.time.LocalDate

data class Customer(
    val id: Long,
    val firstName: String?,
    val lastName: String?,
    val email: String,
    val creationDate: LocalDate = LocalDate.now()
) {

    val addresses = mutableListOf<Address>()

    operator fun plusAssign(address: Address) {
        addresses += address
    }
    fun addAddress(address: Address) {
        addresses += address
    }

    companion object {
        const val TABLE = "customer"
    }
}