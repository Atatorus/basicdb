package fr.atatorus.basicdb.kotlin.models

@JvmRecord
data class Product(val id: Long, val name: String, val description: String?, val price: Double) {

    companion object {
        const val TABLE = "product"
    }
}