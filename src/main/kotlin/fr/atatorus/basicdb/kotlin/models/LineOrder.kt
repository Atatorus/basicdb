package fr.atatorus.basicdb.kotlin.models

@JvmRecord
data class LineOrder(val id: Long, val rank: Int, val productId: Long, val quantity: Int, val orderId: Long) {

    companion object {
        const val TABLE = "line_order"
    }
}