package fr.atatorus.basicdb.kotlin.models

import java.time.LocalDate

@JvmRecord
data class Order(val id: Long, val orderDate: LocalDate, val deliveryAddressId: Long) {

    companion object {
        const val TABLE = "orders"
    }
}