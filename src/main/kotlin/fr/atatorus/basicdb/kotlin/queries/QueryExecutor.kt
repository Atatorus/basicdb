package fr.atatorus.basicdb.kotlin.queries

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*

/**
 * Exécute une requête INSERT
 *
 * @param sql        La requête à exécuter
 * @param parameters les paramètres de la requête
 * @param key        le nom de la clé primaire auto générée
 * @return la clé primaire auto générée
 */
@Throws(SQLException::class, BasicdbException::class)
fun Connection.insertQuery(sql: String, key: String, vararg parameters: Any?) =
    statement(sql, key, parameters.asList()).use { stmt ->
        stmt.executeUpdate()
        stmt.generatedKeys.use { rs ->
            if (rs.next()) {
                rs.getLong(key)
            } else {
                throw BasicdbException("Insert error")
            }
        }
    }

/**
 * Execute une requête UPDATE ou DELETE
 *
 * @param sql        la requête a exécuter
 * @param parameters les paramètres
 * @return le nombre d'enregistrements mis à jour
 * @throws SQLException en cas d'erreur SQL
 */
@Throws(SQLException::class)
fun Connection.updateQuery(sql: String, vararg parameters: Any?): Int {
    statement(sql, parameters.asList()).use { stmt -> return stmt.executeUpdate() }
}

@Throws(SQLException::class)
fun <T> Connection.queryAndMapObjects(
    sql: String,
    mapper: (ResultSet) -> T,
    vararg parameters: Any?
): List<T> {
    statement(sql, parameters.asList()).use { stmt ->
        val objects: MutableList<T> = ArrayList()
        stmt.executeQuery().use { rs ->
            while (rs.next()) {
                objects.add(mapper.invoke(rs))
            }
        }
        return objects
    }
}

@Throws(SQLException::class)
fun <T> Connection.queryAndExtractObjects(
    sql: String,
    extractor: (ResultSet) -> List<T>,
    vararg parameters: Any?
): List<T> {
    statement(sql, parameters.asList()).use { stmt ->
        return stmt.executeQuery().use { rs ->
            extractor.invoke(rs)
        }
    }
}

@Throws(SQLException::class)
private fun Connection.statement(
    sql: String,
    parameters: List<Any?>
) = prepareStatement(
    sql,
    ResultSet.TYPE_SCROLL_INSENSITIVE,
    ResultSet.CONCUR_READ_ONLY,
    ResultSet.CLOSE_CURSORS_AT_COMMIT
).apply {
    parameters.forEachIndexed { ix, parameter ->
        setObject(ix + 1, parameter)
    }
}

@Throws(SQLException::class)
private fun Connection.statement(
    sql: String,
    key: String,
    parameters: List<Any?>
) = prepareStatement(sql, arrayOf(key)).apply {
    parameters.forEachIndexed { ix, parameter ->
        setObject(ix + 1, parameter)
    }
}
