package fr.atatorus.basicdb.kotlin.exceptions

class BasicdbException : Exception {
    constructor(cause: Throwable?) : super(cause)
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
}