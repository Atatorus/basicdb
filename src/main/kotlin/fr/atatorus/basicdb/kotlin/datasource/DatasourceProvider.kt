package fr.atatorus.basicdb.kotlin.datasource

import javax.sql.DataSource

class DatasourceProvider<T: DataSource>(val datasource: T, initialize: T.() -> Unit) {

    init {
        datasource.initialize()
    }
}