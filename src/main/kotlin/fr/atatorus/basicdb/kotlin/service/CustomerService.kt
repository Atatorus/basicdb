package fr.atatorus.basicdb.kotlin.service

import fr.atatorus.basicdb.kotlin.exceptions.BasicdbException
import fr.atatorus.basicdb.kotlin.dao.insertAddress
import fr.atatorus.basicdb.kotlin.dao.insertCustomer
import fr.atatorus.basicdb.kotlin.models.Address
import fr.atatorus.basicdb.kotlin.models.Customer
import fr.atatorus.basicdb.kotlin.tx.IsolationLevel
import fr.atatorus.basicdb.kotlin.tx.TransactionProvider
import java.time.LocalDate
import java.util.*
import fr.atatorus.basicdb.kotlin.dao.deleteCustomer as deleteCustomerDao
import fr.atatorus.basicdb.kotlin.dao.findCustomer as findCustomerDao
import fr.atatorus.basicdb.kotlin.dao.findCustomerWithAddresses as findCustomerDaoWithAddressesDao
import fr.atatorus.basicdb.kotlin.dao.updateCustomer as updateCustomerDao

/**
 * CRUD operations sur les [Customer]s
 */
class CustomerService(private val txProvider: TransactionProvider) {
    @Throws(BasicdbException::class)
    fun createCustomer(
        firstname: String?,
        lastName: String?,
        email: String,
        creationDate: LocalDate
    ): Result<Customer> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            insertCustomer(firstname, lastName, email, creationDate)
        }
    }

    @Throws(BasicdbException::class)
    fun updateCustomer(
        customer: Customer,
        newFirstName: String?,
        newLastName: String?,
        newEmail: String
    ): Result<Customer> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            updateCustomerDao(customer, newFirstName, newLastName, newEmail)
        }
    }

    @Throws(BasicdbException::class)
    fun findCustomer(email: String): Result<Customer?> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            findCustomerDao(email)
        }
    }

    @Throws(BasicdbException::class)
    fun deleteCustomer(customer: Customer) {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        tx.execute {
            deleteCustomerDao(customer)
        }
    }

    @Throws(BasicdbException::class)
    fun createCustomerWithAddress(
        firstName: String?,
        lastName: String?,
        email: String,
        creationDate: LocalDate,
        street: String,
        zip: String,
        city: String,
        country: String?
    ): Result<Customer> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            val customer = insertCustomer(firstName, lastName, email, creationDate)
            val address = insertAddress(street, zip, city, country, customer.id)
            customer.addAddress(address)
            customer
        }
    }

    @Throws(BasicdbException::class)
    fun createAddress(
        customer: Customer,
        street: String?,
        zip: String?,
        city: String?,
        country: String?
    ): Result<Address> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            insertAddress(street!!, zip!!, city!!, country, customer.id)
        }
    }

    @Throws(BasicdbException::class)
    fun findCustomerWithAddresses(email: String): Result<Customer?> {
        val tx = txProvider.getTransaction(false, IsolationLevel.READ_COMMITTED)
        return tx.execute {
            findCustomerDaoWithAddressesDao(email).firstOrNull()
        }
    }

}