create table customer (
	id bigserial primary key,
	first_name varchar,
	last_name varchar,
	email varchar not null,
	creation_date date not null
);
create unique index udx_customer_email on customer(email);

create table address (
	id bigserial primary key,
	street varchar not null,
	zip varchar not null,
	city varchar not null,
	country varchar,
	customer_id bigint not null references customer(id)
);
create table product (
	id bigserial primary key,
	name varchar not null,
	description varchar,
	price double precision
);
create table orders(
	id bigserial primary key,
	order_date date not null,
	delivery_address_id bigint not null references address(id)
);
create table line_order (
	id bigserial primary key,
	rank int not null,
	product_id bigint not null references product(id),
	quantity int not null,
	orderId bigint not null references orders(id)
);
