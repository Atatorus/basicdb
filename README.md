# basicdb

## Description

`basicdb` est un projet permettant l'accès à une base de données. Il a pour but de servir de support à une présentation
montrant le passage de la programmation objet en Java à l'équivalent en programmation fonctionnelle en Kotlin.

## Architecture

On utilise les briques de base de JDBC pour accéder à la base de
données : `javax.sql.DataSource`, `java.sql.Connection`, `java.sql.PreparedStatement` et `java.sql.ResultSet`.

La base de données est PostgreSQL.

### Modèles

Seules deux tables sont utilisées.

    +-----------------------------+                   +---------------------------+
    |  customer                   |                   |  address                  |
    +-----------------------------+                   +---------------------------+
    | id bigint pk                |<----+             | id bigserial pk           |
    | first_name varchar          |     |             | street varchar not null   |
    | last_name varchar           |     |             | zip varchar not null      |
    | email varchar not null      |     |             | city varchar not null     |
    | creation_date date not null |     |             | country varchar           |
    +-----------------------------+     +-------------| customer_id bigint        |
                                                      +---------------------------+

Pour chacune, nous avons 2 records : `fr.atatorus.basicdb.models.Customer`
et `fr.atatorus.basicdb.models.Address`

### Connection à la base de données

Les objets concernés se trouvent dans les packages `fr.atatorus.basicdb.datasource` pour les connections et
`fr.atatorus.basicdb.models.tx` pour les transactions.

### Exécution des requêtes

On utilise les Data Access Objects présents dans le package `fr.atatorus.basicdb.dao`. Pour exécuter les requêtes, ces
objets utilisent un objet `fr.atatorus.basicdb.queries.QueryExecutor`. Ce dernier a pour rôle de créer
les `java.sql.PreparedStatement`, puis exécuter la requête. Le `java.sql.ResultSet` obtenu sera transformé en modèle à
l'aide des `fr.atatorus.basicdb.mapper.Mapper` et `fr.atatorus.basicdb.mapper.Extractor`.

### Gestion des transactions

Pour ceci, nous disposons d'un `fr.atatorus.basicdb.tx.TransactionProvider` qui nous fournit
un `fr.atatorus.basicdb.tx.Transaction`. C'est dans le `TransactionProvider` qui ouvre `Connection`, qu'il place
dans la `Transaction`.

La gestion des transactions est effectuée dans l'objet `fr.atatorus.basicdb.service.CustomerService`. C'est lui qui
demande une transaction au provider, et il a la charge de la committer ou la rollbacker selon les cas.

## Licence

Tout le code du projet est sous [licence Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0). 

